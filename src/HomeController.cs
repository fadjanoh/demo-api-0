﻿using Microsoft.AspNetCore.Mvc;

namespace demo_api.Controllers
{
    public class Home
    {
        public string Api { get; set; }
        public string Version { get; set; }
    }

    [ApiController]
    [Route("/")]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public Home Get()
        {
            return new Home { Api = "Demo API", Version = "0.1.0" };
        }
    }
}
